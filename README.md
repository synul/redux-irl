# Redux IRL
*Told Redux to come fight me IRL. I won.*

## TODO
- Incorporate 'extraReducers'
- Logger?
- Verify https://redux-toolkit.js.org/usage/usage-with-typescript#wrapping-createslice
- Add tests
- Figure out how to communicate throw in reducers when wrong meta is supplied
- Add sorting to reducers via meta
- Preprocessors?
- Type/communicate correctly which reducers need 'idKey'
- Solve @typescript-eslint/no-explicit-any