import {
  createEntityAdapter,
  createSlice,
  Draft,
  IdSelector,
  PayloadAction,
  Dispatch,
  Update,
} from '@reduxjs/toolkit';

import reduxIrl from '.';


export interface RepoOpts<Entity> {
  name: string,
  selectId?: IdSelector<Entity>,
  extraReducers?: RepoExtraReducersMap<EntityState<Entity>>,
}

export enum ReduceOperation {
  AddMany = 'AddMany',
  AddOne = 'AddOne',
  RemoveAll = 'RemoveAll',
  RemoveMany = 'RemoveMany',
  RemoveOne = 'RemoveOne',
  SetAll = 'SetAll',
  UpdateMany = 'UpdateMany',
  UpdateOne = 'UpdateOne',
  UpsertMany = 'UpsertMany',
  UpsertOne = 'UpsertOne',
}

export type RepoReducerFn<S, P> = (state: Draft<S>, action: PayloadAction<P>) => void;
export type RepoExtraReducersMap<State> = {
  [K: string]: RepoReducerFn<State, unknown>;
}

type RepoFetchOpts<FetchResult> = {
  fetchFn: (...args: unknown[]) => Promise<FetchResult>;
  operation: ReduceOperation;
  processor?: (result: FetchResult, state: unknown) => FetchResult;
  fetchFnArgs?: unknown[];
}

type EntityId = string | number;

interface EntityState<T> {
  entities: Record<EntityId, T>;
  ids: EntityId[];
  loading: 'idle' | 'pending';
}


const log = {
  err: (msg: string) => {
    console.error('[redux-irl] Error: ', msg);
  }
}

function isEntityId(test: unknown): test is EntityId {
  return (
    typeof test === 'number'
    || typeof test === 'string'
  );
}

function isEntityIdArray(test: unknown): test is EntityId[] {
  return (
    Array.isArray(test)
    && test.every(item => isEntityId(item))
  )
}


/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export function createRepo<Entity extends any>({
  name,
  selectId,
  extraReducers,
}: RepoOpts<Entity>) {

  const adapter = createEntityAdapter({
    selectId: selectId ? selectId : (entity: Entity) => entity.Guid,
    // Keep the "all IDs" array sorted based on book titles
    // sortComparer: (a, b) => a.title.localeCompare(b.title)
  });

  const initEntityState: EntityState<Entity> = {
    entities: {},
    ids: [],
    loading: 'idle',
  }

  const slice = createSlice({
    extraReducers,
    name,
    initialState: adapter.getInitialState(initEntityState),
    reducers: {
      addMany: adapter.addMany,
      addOne: adapter.addOne,
      removeAll: adapter.removeAll,
      removeMany: adapter.removeMany,
      removeOne: adapter.removeOne,
      setAll: adapter.setAll,
      updateMany: adapter.updateMany,
      updateOne: adapter.updateOne,
      upsertMany: adapter.upsertMany,
      upsertOne: adapter.upsertOne,
      setIdle: state => {
        state.loading = 'idle';
      },
      setPending: state => {
        state.loading = 'pending';
      },
    },
  });

  const fetch = <State, Result extends EntityId | EntityId[] | Entity | Entity[]>({
    fetchFn,
    fetchFnArgs = [],
    operation,
    processor,
  }: RepoFetchOpts<Result>) => {
    return async (dispatch: Dispatch, getState: () => State) => {

      try {

        dispatch(slice.actions.setPending());
        let res = await fetchFn(...fetchFnArgs);

        res = reduxIrl.defaultProcessor(res);

        if (processor) {
          res = processor(res, getState());
        }

        switch(operation) {
          case ReduceOperation.AddMany: {
            if (!Array.isArray(res)) { log.err('Result must be of type Entity[].'); return; }
            dispatch(slice.actions.addMany(res));
            break;
          }
          case ReduceOperation.AddOne: {
            dispatch(slice.actions.addOne(res));
            break;
          }
          case ReduceOperation.RemoveAll: {
            if (typeof res === 'boolean') { log.err('Result must be of type boolean.'); return; }
            if (!res) { log.err('Result must be true.'); return; }
            dispatch(slice.actions.removeAll());
            break;
          }
          case ReduceOperation.RemoveMany: {
            if (!isEntityIdArray(res)) { log.err('Result must be of type EntityId[].'); return; }
            dispatch(slice.actions.removeMany(res));
            break;
          }
          case ReduceOperation.RemoveOne: {
            if (!isEntityId(res)) { log.err('Result must be of type EntityId.'); return; }
            dispatch(slice.actions.removeOne(res));
            break;
          }
          case ReduceOperation.SetAll: {
            if (!Array.isArray(res)) { log.err('Result must be of type Entity[].'); return; }
            dispatch(slice.actions.setAll(res));
            break;
          }
          case ReduceOperation.UpdateMany: {
            if (!Array.isArray(res)) { log.err('Result must be of type Update<EntityId>[].'); return; }
            dispatch(slice.actions.updateMany(res));
            break;
          }
          case ReduceOperation.UpdateOne: {
            dispatch(slice.actions.updateOne(res as unknown as Update<Entity>));
            break;
          }
          case ReduceOperation.UpsertMany: {
            if (!Array.isArray(res)) { log.err('Result must be of type Entity[].'); return; }
            dispatch(slice.actions.upsertMany(res));
            break;
          }
          case ReduceOperation.UpsertOne: {
            dispatch(slice.actions.upsertOne(res));
            break;
          }
        }

        return res;

      } catch (err) {

        reduxIrl.errorHandler(err);
        dispatch(slice.actions.setIdle());
        return err;

      }
    }
  }

  return {
    adapter,
    fetch,
    slice,
  }
}
