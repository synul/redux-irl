/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  AnyObject,
  DataState,
} from '.';

export function isDataState(test: any): test is DataState<unknown> {
  return (
    Object.prototype.hasOwnProperty.call(test, 'status')
    && Object.prototype.hasOwnProperty.call(test, 'data')
  );
}

export function isArrayDataState(test: any): test is DataState<AnyObject[]> {
  return isDataState(test) && Array.isArray(test.data);
}

export function isObjectDataState(test: any): test is DataState<AnyObject> {
  return isDataState(test) && typeof test.data === 'object' && !Array.isArray(test.data);
}

export function isArrayPayload(test: any): test is { payload: AnyObject[] } {
  return (
    Object.prototype.hasOwnProperty.call(test, 'payload')
    && Array.isArray(test.payload)
  );
}

export function isObjectPayload(test: any): test is { payload: AnyObject } {
  return (
    Object.prototype.hasOwnProperty.call(test, 'payload')
    && typeof test.payload === 'object'
    && !Array.isArray(test.payload)
  );
}

export function isStringPayload(test: any): test is { payload: string } {
  return (
    Object.prototype.hasOwnProperty.call(test, 'payload')
    && typeof test.payload === 'string'
  );
}

export function isStringArrayPayload(test: any): test is { payload: string[] } {
  return (
    Object.prototype.hasOwnProperty.call(test, 'payload')
    && Array.isArray(test.payload)
    && !test.payload.find((item: unknown) => typeof item !== 'string')
  );
}