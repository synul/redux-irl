import { makeSlice } from '..';

import { initDataState, testSetup } from '../utils';


describe('update reducer', () => {
  const { mockMeta, obj1, obj1Updated, obj2, obj3 } = testSetup();

  describe('when using array type slice', () => {
    const { actions, reducer } = makeSlice('quotes', []);
    const initial = initDataState([obj1, obj2]);
  
    describe('manyToArr', () => {
      it('should add data correctly', () => {
        const res = [obj3];
        const test = reducer(initial, actions.update(res, mockMeta));
        expect(test.data.length).toBe(3);
        expect(test.data).toContain(obj1);
        expect(test.data).toContain(obj2);
      });
  
      it('should not duplicate data', () => {
        const res = [obj1Updated, obj3];
        const test = reducer(initial, actions.update(res, mockMeta))
  
        expect(test.data.length).toBe(3);
        expect(test.data).toContainEqual(obj1Updated);
        expect(test.data).toContain(obj2);
        expect(test.data).toContain(obj3);
      });
  
      // it('complains about missing idKey', () => {
      //   const res = [obj1Updated, obj3];
      //   reducer(initDataState([obj1, obj2]), actions.update(res));
      //   beforeEach(() => {
      //     jest.spyOn(console, 'error')
      //   });
      //   const spy = jest.spyOn(console, 'error');
      //   expect(spy).toHaveBeenCalled();
      // });
    });
  
    describe('oneToArr', () => {
      it('should add data correctly', () => {
        const test = reducer(initial, actions.update(obj3, mockMeta));
        expect(test.data.length).toBe(3);
        expect(test.data).toContain(obj1);
        expect(test.data).toContain(obj2);
        expect(test.data).toContain(obj3);
      });
  
      it('should not duplicate data', () => {
        const test = reducer(initial, actions.update(obj1Updated, mockMeta));
        expect(test.data.length).toBe(2);
        expect(test.data).toContainEqual(obj1Updated);
        expect(test.data).toContain(obj2);
      });
    });
  });
  
  describe('when using object type slice', () => {
    const { actions, reducer } = makeSlice('quotes', {});
    const initial = initDataState({ [obj1.id]: obj1 });
  
    describe('manyToObj', () => {
      it('should correctly add data', () => {
        const res = [obj1, obj2];
        const test = reducer(initial, actions.update(res, mockMeta));
        expect(Object.keys(test.data).length).toBe(2);
        expect(test.data).toHaveProperty(obj1.id);
        expect(test.data).toHaveProperty(obj2.id);
      });
  
      it('should not duplicate data', () => {
        const res = [obj1Updated, obj2];
        const test = reducer(initial, actions.update(res, mockMeta));
        expect(test.data).toHaveProperty(obj1.id);
        expect(test.data[obj1.id]).toStrictEqual(obj1Updated);
      });
    });
  
    describe('oneToObj', () => {
      it('should correctly add data', () => {
        const test = reducer(initial, actions.update(obj2, mockMeta));
        expect(Object.keys(test.data).length).toBe(2);
        expect(test.data).toHaveProperty(obj1.id);
        expect(test.data).toHaveProperty(obj2.id);
      });
  
      it('should not duplicate data', () => {
        const test = reducer(initial, actions.update(obj1Updated, mockMeta));
        expect(Object.keys(test.data).length).toBe(1);
        expect(test.data).toHaveProperty(obj1.id);
        expect(test.data[obj1.id]).toStrictEqual(obj1Updated);
      });
    });
  });
});