import { Draft, PayloadAction } from '@reduxjs/toolkit';

import {
  AnyObject,
  ActionMeta,
  DataState,
} from '../';
import { setIdle } from './';


export function manyToArr(
  state: Draft<DataState<AnyObject[]>>,
  action: PayloadAction<AnyObject[], string, ActionMeta, boolean>
) {
  const { meta: { idKey }, payload } = action;
  if (!idKey) { console.error('[redux-irl]: No \'idKey\' set.'); return; }

  payload.forEach(newItem => {
    const idx = state.data.findIndex(oldItem => oldItem[idKey] === newItem[idKey]);
    if (idx !== -1) {
      state.data[idx] = newItem;
    } else {
      state.data.push(newItem);
    }
  });

  setIdle(state);
}

export function manyToObj(
  state: Draft<DataState<AnyObject>>,
  action: PayloadAction<AnyObject[], string, ActionMeta, boolean>
) {
  const { meta: { idKey }, payload } = action;
  if (!idKey) { console.error('[redux-irl]: No \'idKey\' set.'); return; }

  payload.forEach(item => {
    state.data[item[idKey]] = item;
  });

  setIdle(state);
}

export function oneToArr(
  state: Draft<DataState<AnyObject[]>>,
  action: PayloadAction<AnyObject, string, ActionMeta, boolean>
) {
  const { meta: { idKey }, payload } = action;
  if (!idKey) { console.error('[redux-irl]: No \'idKey\' set.'); return; }

  const idx = state.data.findIndex(item => item[idKey] === payload[idKey]);
  if (idx !== -1) {
    state.data[idx] = payload;
  } else {
    state.data.push(payload);
  }

  setIdle(state);
}

export function oneToObj(
  state: Draft<DataState<AnyObject>>,
  action: PayloadAction<AnyObject, string, ActionMeta, boolean>
) {
  const { meta: { idKey }, payload } = action;
  if (!idKey) { console.error('[redux-irl]: No \'idKey\' set.'); return; }

  state.data[payload[idKey]] = payload;

  setIdle(state);
}
