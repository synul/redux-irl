import { Draft, PayloadAction } from '@reduxjs/toolkit';

import {
  AnyObject,
  ActionMeta,
  DataState,
} from '../';
import { setIdle } from './';

export function manyFromArr<
  S extends Draft<DataState<AnyObject[]>>,
  R extends string[]
>(
  state: S,
  action: PayloadAction<R, string, ActionMeta, boolean>
) {
  const { meta: { idKey }, payload } = action;
  if (!idKey) { console.error('[redux-irl]: No \'idKey\' set.'); return; }

  const draft = state.data.filter(item => !payload.includes(item[idKey]));
  state.data = draft;

  setIdle(state);
}

export function manyFromObj<
  S extends Draft<DataState<AnyObject>>,
  R extends string[]
>(
  state: S,
  action: PayloadAction<R, string, ActionMeta, boolean>
) {
  // const { meta: { idKey }, payload } = action;
  // if (!idKey) { console.error('[redux-irl]: No \'idKey\' set.'); return; }
  action.payload.forEach(id => {
    delete state.data[id];
  });

  setIdle(state);
}

export function oneFromArr<
  S extends Draft<DataState<AnyObject[]>>,
  R extends string
>(
  state: S,
  action: PayloadAction<R, string, ActionMeta, boolean>
) {
  const { meta: { idKey }, payload } = action;
  if (!idKey) { console.error('[redux-irl]: No \'idKey\' set.'); return; }

  const draft = state.data.filter(item => item[idKey] !== payload);
  state.data = draft;

  setIdle(state);
}

export function oneFromObj<
  S extends Draft<DataState<AnyObject>>,
  R extends string
>(
  state: S,
  action: PayloadAction<R, string, ActionMeta, boolean>
) {
  // const { meta: { idKey }, payload } = action;
  // if (!idKey) { console.error('[redux-irl]: No \'idKey\' set.'); return; }
  delete state.data[action.payload];

  setIdle(state);
}
