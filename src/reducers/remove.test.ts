import { makeSlice } from '..';

import { initDataState, testSetup } from '../utils';


describe('remove reducer', () => {
  const { mockMeta, obj1, obj2, obj3 } = testSetup();
  
  describe('when using array type slice', () => {
    const { actions, reducer } = makeSlice('quotes', []);
    const initial = initDataState([obj1, obj2, obj3]);
  
    describe('manyFromArr', () => {
      it('should correctly remove data', () => {
        const test = reducer(initial, actions.remove([obj1.id, obj2.id], mockMeta));
        expect(Object.keys(test.data).length).toBe(1);
        expect(test.data).not.toContain(obj1);
        expect(test.data).not.toContain(obj2);
        expect(test.data).toContain(obj3);
      });
    });
  
    describe('oneFromArr', () => {
      it('should correctly remove data', () => {
        const test = reducer(initial, actions.remove(obj1.id, mockMeta));
        expect(Object.keys(test.data).length).toBe(2);
        expect(test.data).not.toContain(obj1);
        expect(test.data).toContain(obj2);
        expect(test.data).toContain(obj3);
      });
    });
  });
  
  describe('when using object type slice', () => {
    const { actions, reducer } = makeSlice('quotes', {});
    const initial = initDataState({ [obj1.id]: obj1, [obj2.id]: obj2, [obj3.id]: obj3 });
  
    describe('manyFromObj', () => {
      it('should correctly remove data', () => {
        const test = reducer(initial, actions.remove([obj1.id, obj2.id], mockMeta))
        expect(Object.keys(test.data).length).toBe(1);
        expect(test.data).not.toHaveProperty(obj1.id);
        expect(test.data).not.toHaveProperty(obj2.id);
        expect(test.data).toHaveProperty(obj3.id);
      });
    });
  
    describe('oneFromObj', () => {
      it('should correctly remove data', () => {
        const test = reducer(initial, actions.remove(obj1.id, mockMeta))
        expect(Object.keys(test.data).length).toBe(2);
        expect(test.data).not.toHaveProperty(obj1.id);
        expect(test.data).toHaveProperty(obj2.id);
        expect(test.data).toHaveProperty(obj3.id);
      });
    });
  });
});