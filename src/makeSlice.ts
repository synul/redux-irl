/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  CaseReducerWithPrepare,
  createSlice,
  Draft,
  PayloadAction,
  // Slice,
  // SliceCaseReducers,
  // CaseReducer,
} from '@reduxjs/toolkit'

import {
  ActionMeta,
  DataState,
} from '.';
import {
  manyFromArr,
  manyFromObj,
  manyToArr,
  manyToObj,
  oneFromArr,
  oneFromObj,
  oneToArr,
  oneToObj,
  setFailed,
  setPending,
} from './reducers';
import {
  isArrayDataState,
  isArrayPayload,
  isObjectDataState,
  isObjectPayload,
  isStringArrayPayload,
  isStringPayload,
} from './typeGuards';
import { createStandardFluxAction, initDataState } from './utils';


/**
 * Specialisation of @reduxjs/toolkit's `SliceCaseReducers<State>`, representing the (only) case reducers `makeSlice`
 * will produce.
 */
// eslint-disable-next-line
// @ts-ignore
export type SpecializedSliceCaseReducers<State extends any> = {
  // TODO: Type this so State will be passed through reducers.
  remove: CaseReducerWithPrepare<any, FsaPayloadAction>;
  setPending: CaseReducerWithPrepare<any, FsaPayloadAction>;
  setFailed: CaseReducerWithPrepare<any, FsaPayloadAction>;
  update: CaseReducerWithPrepare<any, FsaPayloadAction>;
  [K: string]: CaseReducerWithPrepare<any, FsaPayloadAction>;
}

export type FsaPayloadAction<P = any> = PayloadAction<P, string, ActionMeta, boolean>;
export type ReducerFunction<S, P> = (state: Draft<S>, action: FsaPayloadAction<P>) => void;
export type ExtraReducersMap<S> = {
  [K: string]: ReducerFunction<DataState<S>, any>;
}

export function makeSlice<State extends any>(
  name: string,
  initialState: State,
  extraReducers?: ExtraReducersMap<State>,
) {

  // CREATE, UPDATE
  function update<S extends DataState<any>>(state: S, action: FsaPayloadAction) {
    if (!action.payload) {
      console.error('[redux-irl]: Can\'t choose reducer for null or undefined payload.')
    }

    if (isObjectDataState(state) && isObjectPayload(action)) {
      return oneToObj(state, action);
    } else if (isObjectDataState(state) && isArrayPayload(action)) {
      return manyToObj(state, action);
    } else if (isArrayDataState(state) && isObjectPayload(action)) {
      return oneToArr(state, action);
    } else if (isArrayDataState(state) && isArrayPayload(action)) {
      return manyToArr(state, action);
    }
  }

  // DELETE
  function remove<S extends DataState<S>>(state: S, action: FsaPayloadAction) {
    if (!action.payload) {
      console.error('[redux-irl]: Can\'t choose reducer for null or undefined payload.')
    }

    if (isObjectDataState(state) && isStringPayload(action)) {
      return oneFromObj(state, action);
    } else if (isObjectDataState(state) && isStringArrayPayload(action)) {
      return manyFromObj(state, action);
    } else if (isArrayDataState(state) && isStringPayload(action)) {
      return oneFromArr(state, action);
    } else if (isArrayDataState(state) && isStringArrayPayload(action)) {
      return manyFromArr(state, action);
    }
  }

  const prepare = createStandardFluxAction;

  // reducers: ValidateSliceCaseReducers<State, SliceCaseReducers<State>>
  const reducers = {
    remove: { prepare, reducer: remove },
    setFailed: { prepare, reducer: setFailed },
    setPending: { prepare, reducer: setPending },
    update: { prepare, reducer: update },
  };

  // options: CreateSliceOptions<State, typeof reducers>
  const options = {
    extraReducers,
    initialState: initDataState(initialState),
    name,
    reducers,
  };

  return createSlice<DataState<State>, SpecializedSliceCaseReducers<DataState<State>>>(options);
}
