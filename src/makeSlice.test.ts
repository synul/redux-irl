import { makeSlice } from '.';

import { createStandardFluxAction, initDataState, testSetup } from './utils';
import { isArrayDataState, isDataState } from './typeGuards';


describe('makeSlice', () => {
  const { mockMeta, obj1 } = testSetup();

  describe('when creating slice', () => {
    const initialState = [];
    const { actions, reducer } = makeSlice('quotes', initialState);

    it('should create default crud actions', () => {
      expect(Object.prototype.hasOwnProperty.call(actions, 'setFailed')).toBe(true);
      expect(Object.prototype.hasOwnProperty.call(actions, 'setPending')).toBe(true);
      expect(Object.prototype.hasOwnProperty.call(actions, 'update')).toBe(true);
      expect(Object.prototype.hasOwnProperty.call(actions, 'remove')).toBe(true);
    });

    it('should have the correct action for setFailed', () => {
      expect(actions.setFailed()).toEqual({
        type: 'quotes/setFailed',
        ...createStandardFluxAction(undefined),
      });
    });

    it('should have the correct action for setPending', () => {
      expect(actions.setPending()).toEqual({
        type: 'quotes/setPending',
        ...createStandardFluxAction(undefined),
      });
    });

    it('should have the correct action for update', () => {
      expect(actions.update()).toEqual({
        type: 'quotes/update',
        ...createStandardFluxAction(undefined),
      });
    });

    it('should have the correct action for remove', () => {
      expect(actions.remove()).toEqual({
        type: 'quotes/remove',
        ...createStandardFluxAction(undefined),
      });
    });

    it('should construct correct data state for setFailed', () => {
      const testError = new Error('Test Error');
      const test = reducer(undefined, actions.setFailed(testError));
      expect(isDataState(test)).toBe(true);
      expect(test.data).toEqual(initialState);
      expect(test.error).toEqual(testError);
      expect(test.status).toEqual('idle');
    });

    it('should construct correct data state for setPending', () => {
      const test = reducer(undefined, actions.setPending());
      expect(isDataState(test)).toBe(true);
      expect(test.data).toEqual(initialState);
      expect(test.status).toEqual('pending');
    });

    it('should construct correct data state for update', () => {
      const test = reducer(initDataState([]), actions.update(obj1, mockMeta));
      expect(isArrayDataState(test)).toBe(true);
      expect(test.status).toBe('idle');
      expect(test.data.length).toBe(1);
    });

    it('should construct correct data state for remove', () => {
      const test = reducer(initDataState([obj1]), actions.remove(obj1.id, mockMeta));
      expect(isArrayDataState(test)).toBe(true);
      expect(test.status).toBe('idle');
      expect(test.data.length).toBe(0);
    });
  });

});
