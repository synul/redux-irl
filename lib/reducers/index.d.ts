import { Draft } from '@reduxjs/toolkit';
import { DataState, FsaPayloadAction } from '../';
export * from './update';
export * from './remove';
export declare function setFailed<S extends DataState<S>>(state: S, action: FsaPayloadAction): void;
export declare function setPending<S extends Draft<DataState<S>>>(state: S): void;
export declare function setIdle<S>(dataState: DataState<S>): void;
