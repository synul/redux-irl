import { Draft, PayloadAction } from '@reduxjs/toolkit';
import { AnyObject, ActionMeta, DataState } from '../';
export declare function manyFromArr<S extends Draft<DataState<AnyObject[]>>, R extends string[]>(state: S, action: PayloadAction<R, string, ActionMeta, boolean>): void;
export declare function manyFromObj<S extends Draft<DataState<AnyObject>>, R extends string[]>(state: S, action: PayloadAction<R, string, ActionMeta, boolean>): void;
export declare function oneFromArr<S extends Draft<DataState<AnyObject[]>>, R extends string>(state: S, action: PayloadAction<R, string, ActionMeta, boolean>): void;
export declare function oneFromObj<S extends Draft<DataState<AnyObject>>, R extends string>(state: S, action: PayloadAction<R, string, ActionMeta, boolean>): void;
