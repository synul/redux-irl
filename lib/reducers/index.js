export * from './update';
export * from './remove';
export function setFailed(state, action) {
    state.received = Date.now();
    state.status = 'idle';
    state.error = action.payload;
}
// export function setFailedSilently<T>(dataState: DataState<T>) {
//   return dataState.status = 'FailedSilently';
// }
export function setPending(state) {
    state.requested = Date.now();
    state.status = 'pending';
}
export function setIdle(dataState) {
    dataState.received = Date.now();
    dataState.status = 'idle';
}
