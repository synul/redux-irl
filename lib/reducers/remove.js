import { setIdle } from './';
export function manyFromArr(state, action) {
    const { meta: { idKey }, payload } = action;
    if (!idKey) {
        console.error('[redux-irl]: No \'idKey\' set.');
        return;
    }
    const draft = state.data.filter(item => !payload.includes(item[idKey]));
    state.data = draft;
    setIdle(state);
}
export function manyFromObj(state, action) {
    // const { meta: { idKey }, payload } = action;
    // if (!idKey) { console.error('[redux-irl]: No \'idKey\' set.'); return; }
    action.payload.forEach(id => {
        delete state.data[id];
    });
    setIdle(state);
}
export function oneFromArr(state, action) {
    const { meta: { idKey }, payload } = action;
    if (!idKey) {
        console.error('[redux-irl]: No \'idKey\' set.');
        return;
    }
    const draft = state.data.filter(item => item[idKey] !== payload);
    state.data = draft;
    setIdle(state);
}
export function oneFromObj(state, action) {
    // const { meta: { idKey }, payload } = action;
    // if (!idKey) { console.error('[redux-irl]: No \'idKey\' set.'); return; }
    delete state.data[action.payload];
    setIdle(state);
}
