export * from './fetchHandler';
export * from './makeSlice';
export * from './createRepo';
export var ReducerOp;
(function (ReducerOp) {
    ReducerOp["Update"] = "Update";
    ReducerOp["Delete"] = "Delete";
})(ReducerOp || (ReducerOp = {}));
export function unpack(dataState) {
    return dataState.data;
}
class ReduxIrl {
    constructor() {
        this.defaultProcessor = (res) => res;
        this.errorHandler = (err) => { console.error('[redux-irl] Error: ', err); };
    }
    init({ defaultProcessor, errorHandler, }) {
        if (defaultProcessor) {
            this.defaultProcessor = defaultProcessor;
        }
        if (errorHandler) {
            this.errorHandler = errorHandler;
        }
    }
}
const reduxIrl = new ReduxIrl();
export default reduxIrl;
