import { CaseReducerWithPrepare, Draft, PayloadAction } from '@reduxjs/toolkit';
import { ActionMeta, DataState } from '.';
/**
 * Specialisation of @reduxjs/toolkit's `SliceCaseReducers<State>`, representing the (only) case reducers `makeSlice`
 * will produce.
 */
export declare type SpecializedSliceCaseReducers<State extends any> = {
    remove: CaseReducerWithPrepare<any, FsaPayloadAction>;
    setPending: CaseReducerWithPrepare<any, FsaPayloadAction>;
    setFailed: CaseReducerWithPrepare<any, FsaPayloadAction>;
    update: CaseReducerWithPrepare<any, FsaPayloadAction>;
    [K: string]: CaseReducerWithPrepare<any, FsaPayloadAction>;
};
export declare type FsaPayloadAction<P = any> = PayloadAction<P, string, ActionMeta, boolean>;
export declare type ReducerFunction<S, P> = (state: Draft<S>, action: FsaPayloadAction<P>) => void;
export declare type ExtraReducersMap<S> = {
    [K: string]: ReducerFunction<DataState<S>, any>;
};
export declare function makeSlice<State extends any>(name: string, initialState: State, extraReducers?: ExtraReducersMap<State>): import("@reduxjs/toolkit").Slice<DataState<State>, SpecializedSliceCaseReducers<DataState<State>>, string>;
