import { DataState, ActionMeta } from '.';
export declare function initDataState<T>(data: T): DataState<T>;
/**
 * Redux toolkit's prepare function returning a Standard Flux Action with `meta` being our own `ActionMeta`.
 * https://github.com/redux-utilities/flux-standard-action
 */
export declare function createStandardFluxAction<T>(payload: T, meta?: ActionMeta): {
    payload: T;
    error: boolean;
    meta: ActionMeta;
};
export declare function testSetup(): {
    mockMeta: {
        idKey: string;
    };
    obj1: {
        id: string;
        value: boolean;
    };
    obj1Updated: {
        value: boolean;
        id: string;
    };
    obj2: {
        id: string;
    };
    obj3: {
        id: string;
    };
};
