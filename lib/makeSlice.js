/* eslint-disable @typescript-eslint/no-explicit-any */
import { createSlice, } from '@reduxjs/toolkit';
import { manyFromArr, manyFromObj, manyToArr, manyToObj, oneFromArr, oneFromObj, oneToArr, oneToObj, setFailed, setPending, } from './reducers';
import { isArrayDataState, isArrayPayload, isObjectDataState, isObjectPayload, isStringArrayPayload, isStringPayload, } from './typeGuards';
import { createStandardFluxAction, initDataState } from './utils';
export function makeSlice(name, initialState, extraReducers) {
    // CREATE, UPDATE
    function update(state, action) {
        if (!action.payload) {
            console.error('[redux-irl]: Can\'t choose reducer for null or undefined payload.');
        }
        if (isObjectDataState(state) && isObjectPayload(action)) {
            return oneToObj(state, action);
        }
        else if (isObjectDataState(state) && isArrayPayload(action)) {
            return manyToObj(state, action);
        }
        else if (isArrayDataState(state) && isObjectPayload(action)) {
            return oneToArr(state, action);
        }
        else if (isArrayDataState(state) && isArrayPayload(action)) {
            return manyToArr(state, action);
        }
    }
    // DELETE
    function remove(state, action) {
        if (!action.payload) {
            console.error('[redux-irl]: Can\'t choose reducer for null or undefined payload.');
        }
        if (isObjectDataState(state) && isStringPayload(action)) {
            return oneFromObj(state, action);
        }
        else if (isObjectDataState(state) && isStringArrayPayload(action)) {
            return manyFromObj(state, action);
        }
        else if (isArrayDataState(state) && isStringPayload(action)) {
            return oneFromArr(state, action);
        }
        else if (isArrayDataState(state) && isStringArrayPayload(action)) {
            return manyFromArr(state, action);
        }
    }
    const prepare = createStandardFluxAction;
    // reducers: ValidateSliceCaseReducers<State, SliceCaseReducers<State>>
    const reducers = {
        remove: { prepare, reducer: remove },
        setFailed: { prepare, reducer: setFailed },
        setPending: { prepare, reducer: setPending },
        update: { prepare, reducer: update },
    };
    // options: CreateSliceOptions<State, typeof reducers>
    const options = {
        extraReducers,
        initialState: initDataState(initialState),
        name,
        reducers,
    };
    return createSlice(options);
}
