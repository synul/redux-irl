var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { ReducerOp } from '.';
/**
 * TODO: Add documentation explaining:
 * 'Splitting' part: Split any fetch into Redux actions representing the three states requested, success, failure.
 * 'Caching' part: Deciding if fetch is actually executed based on timestamps and config params.
 */
export function fetchHandler({ apiCall, meta, slice, apiCallArgs = [], reducer = ReducerOp.Update, }) {
    return (dispatch) => __awaiter(this, void 0, void 0, function* () {
        try {
            dispatch(slice.actions.setPending());
            const res = yield apiCall(...apiCallArgs);
            if (reducer === ReducerOp.Update) {
                dispatch(slice.actions.update(res, meta));
            }
            else if (reducer === ReducerOp.Delete) {
                dispatch(slice.actions.remove(res));
            }
            return res;
        }
        catch (err) {
            dispatch(slice.actions.setFailed(err));
        }
    });
}
