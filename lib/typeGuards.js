export function isDataState(test) {
    return (Object.prototype.hasOwnProperty.call(test, 'status')
        && Object.prototype.hasOwnProperty.call(test, 'data'));
}
export function isArrayDataState(test) {
    return isDataState(test) && Array.isArray(test.data);
}
export function isObjectDataState(test) {
    return isDataState(test) && typeof test.data === 'object' && !Array.isArray(test.data);
}
export function isArrayPayload(test) {
    return (Object.prototype.hasOwnProperty.call(test, 'payload')
        && Array.isArray(test.payload));
}
export function isObjectPayload(test) {
    return (Object.prototype.hasOwnProperty.call(test, 'payload')
        && typeof test.payload === 'object'
        && !Array.isArray(test.payload));
}
export function isStringPayload(test) {
    return (Object.prototype.hasOwnProperty.call(test, 'payload')
        && typeof test.payload === 'string');
}
export function isStringArrayPayload(test) {
    return (Object.prototype.hasOwnProperty.call(test, 'payload')
        && Array.isArray(test.payload)
        && !test.payload.find((item) => typeof item !== 'string'));
}
