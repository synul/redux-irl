var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { createEntityAdapter, createSlice, } from '@reduxjs/toolkit';
import reduxIrl from '.';
export var ReduceOperation;
(function (ReduceOperation) {
    ReduceOperation["AddMany"] = "AddMany";
    ReduceOperation["AddOne"] = "AddOne";
    ReduceOperation["RemoveAll"] = "RemoveAll";
    ReduceOperation["RemoveMany"] = "RemoveMany";
    ReduceOperation["RemoveOne"] = "RemoveOne";
    ReduceOperation["SetAll"] = "SetAll";
    ReduceOperation["UpdateMany"] = "UpdateMany";
    ReduceOperation["UpdateOne"] = "UpdateOne";
    ReduceOperation["UpsertMany"] = "UpsertMany";
    ReduceOperation["UpsertOne"] = "UpsertOne";
})(ReduceOperation || (ReduceOperation = {}));
const log = {
    err: (msg) => {
        console.error('[redux-irl] Error: ', msg);
    }
};
function isEntityId(test) {
    return (typeof test === 'number'
        || typeof test === 'string');
}
function isEntityIdArray(test) {
    return (Array.isArray(test)
        && test.every(item => isEntityId(item)));
}
/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export function createRepo({ name, selectId, extraReducers, }) {
    const adapter = createEntityAdapter({
        selectId: selectId ? selectId : (entity) => entity.Guid,
    });
    const initEntityState = {
        entities: {},
        ids: [],
        loading: 'idle',
    };
    const slice = createSlice({
        extraReducers,
        name,
        initialState: adapter.getInitialState(initEntityState),
        reducers: {
            addMany: adapter.addMany,
            addOne: adapter.addOne,
            removeAll: adapter.removeAll,
            removeMany: adapter.removeMany,
            removeOne: adapter.removeOne,
            setAll: adapter.setAll,
            updateMany: adapter.updateMany,
            updateOne: adapter.updateOne,
            upsertMany: adapter.upsertMany,
            upsertOne: adapter.upsertOne,
            setIdle: state => {
                state.loading = 'idle';
            },
            setPending: state => {
                state.loading = 'pending';
            },
        },
    });
    const fetch = ({ fetchFn, fetchFnArgs = [], operation, processor, }) => {
        return (dispatch, getState) => __awaiter(this, void 0, void 0, function* () {
            try {
                dispatch(slice.actions.setPending());
                let res = yield fetchFn(...fetchFnArgs);
                res = reduxIrl.defaultProcessor(res);
                if (processor) {
                    res = processor(res, getState());
                }
                switch (operation) {
                    case ReduceOperation.AddMany: {
                        if (!Array.isArray(res)) {
                            log.err('Result must be of type Entity[].');
                            return;
                        }
                        dispatch(slice.actions.addMany(res));
                        break;
                    }
                    case ReduceOperation.AddOne: {
                        dispatch(slice.actions.addOne(res));
                        break;
                    }
                    case ReduceOperation.RemoveAll: {
                        if (typeof res === 'boolean') {
                            log.err('Result must be of type boolean.');
                            return;
                        }
                        if (!res) {
                            log.err('Result must be true.');
                            return;
                        }
                        dispatch(slice.actions.removeAll());
                        break;
                    }
                    case ReduceOperation.RemoveMany: {
                        if (!isEntityIdArray(res)) {
                            log.err('Result must be of type EntityId[].');
                            return;
                        }
                        dispatch(slice.actions.removeMany(res));
                        break;
                    }
                    case ReduceOperation.RemoveOne: {
                        if (!isEntityId(res)) {
                            log.err('Result must be of type EntityId.');
                            return;
                        }
                        dispatch(slice.actions.removeOne(res));
                        break;
                    }
                    case ReduceOperation.SetAll: {
                        if (!Array.isArray(res)) {
                            log.err('Result must be of type Entity[].');
                            return;
                        }
                        dispatch(slice.actions.setAll(res));
                        break;
                    }
                    case ReduceOperation.UpdateMany: {
                        if (!Array.isArray(res)) {
                            log.err('Result must be of type Update<EntityId>[].');
                            return;
                        }
                        dispatch(slice.actions.updateMany(res));
                        break;
                    }
                    case ReduceOperation.UpdateOne: {
                        dispatch(slice.actions.updateOne(res));
                        break;
                    }
                    case ReduceOperation.UpsertMany: {
                        if (!Array.isArray(res)) {
                            log.err('Result must be of type Entity[].');
                            return;
                        }
                        dispatch(slice.actions.upsertMany(res));
                        break;
                    }
                    case ReduceOperation.UpsertOne: {
                        dispatch(slice.actions.upsertOne(res));
                        break;
                    }
                }
                return res;
            }
            catch (err) {
                reduxIrl.errorHandler(err);
                dispatch(slice.actions.setIdle());
                return err;
            }
        });
    };
    return {
        adapter,
        fetch,
        slice,
    };
}
