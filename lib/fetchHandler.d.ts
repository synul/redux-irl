import { Dispatch, Slice } from '@reduxjs/toolkit';
import { ActionMeta, ReducerOp, SpecializedSliceCaseReducers } from '.';
export interface FetchHandlerConfig<State, FetchResult> {
    apiCall: (...args: any) => Promise<FetchResult>;
    slice: Slice<State, SpecializedSliceCaseReducers<State>>;
    meta: ActionMeta;
    apiCallArgs?: any[];
    cachingArgs?: any;
    reducer?: ReducerOp;
}
/**
 * TODO: Add documentation explaining:
 * 'Splitting' part: Split any fetch into Redux actions representing the three states requested, success, failure.
 * 'Caching' part: Deciding if fetch is actually executed based on timestamps and config params.
 */
export declare function fetchHandler<State, FetchResult>({ apiCall, meta, slice, apiCallArgs, reducer, }: FetchHandlerConfig<State, FetchResult>): (dispatch: Dispatch<import("redux").AnyAction>) => Promise<FetchResult | undefined>;
